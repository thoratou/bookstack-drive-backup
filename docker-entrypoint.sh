#!/bin/bash

if [ -z "$PUID" ]; then echo "Missing PUID env variable" && exit 1; fi
if [ -z "$PGID" ]; then echo "Missing PGID env variable" && exit 1; fi

export USER=appuser
export UID=$PUID
export GID=$PGID

echo "Adding host user and group"

GROUP_LINE=$(grep ":$PGID:" /etc/group)
if [ -z "$GROUP_LINE" ]; then
    #no group, create it
        su-exec root addgroup --gid ${PGID} ${USER}
        su-exec root adduser \
		--disabled-password \
		--gecos "" \
		--home "/home/appuser_skicka" \
		--ingroup "$USER" \
		--uid "$PUID" \
		"$USER"
    else
        #existing group, retrieve group name and add to group
        GROUP=$(echo "$GROUP_LINE" | awk -F':' '{print $1}')
        su-exec root adduser \
                --disabled-password \
                --gecos "" \
                --home "/home/appuser_skicka" \
                --ingroup "$GROUP" \
                --uid "$PUID" \
                "$USER"
    fi

su-exec root chown -R appuser:appuser /home/appuser
su-exec root chown -R appuser:appuser /home/appuser_skicka


echo "Adding host GID to docker system group"

DOCKER_SOCKET=/var/run/docker.sock
DOCKER_GROUP=docker

if [ -S ${DOCKER_SOCKET} ]; then
    DOCKER_GID=$(stat -c '%g' ${DOCKER_SOCKET})

    #addgroup is distribution specific
    GROUP_LINE=$(grep ":$DOCKER_GID:" /etc/group)
    if [ -z "$GROUP_LINE" ]; then
	#no group, create it
	su-exec root addgroup --gid ${DOCKER_GID} ${DOCKER_GROUP}
	su-exec root adduser ${USER} ${DOCKER_GROUP}
    else
	#existing group, retrieve group name and add to group
	GROUP=$(echo "$GROUP_LINE" | awk -F':' '{print $1}')
	su-exec root adduser ${USER} ${GROUP}
    fi
fi

su-exec appuser "$@"
