#!/bin/bash

#Are we ok ?
if [ -z "$DRIVE_PATH" ]; then echo "Missing DRIVE_PATH env variable" && exit 1; fi
if [ -z "$MAX_BACKUP_NUMBER" ]; then echo "Missing MAX_BACKUP_NUMBER env variable" && exit 1; fi
if [ -z "$DB_USER" ]; then echo "Missing DB_USER env variable" && exit 1; fi
if [ -z "$DB_PASSWORD" ]; then echo "Missing DB_PASSWORD env variable" && exit 1; fi
if [ -z "$DB_DATABASE" ]; then echo "Missing DB_DATABASE env variable" && exit 1; fi

DATE=$(date "+%Y%m%d.%H%M%S")

mkdir -p /home/appuser/$DRIVE_PATH/$DATE

#DB backup
docker exec -i bookstack_db mysqldump -u $DB_USER -p$DB_PASSWORD $DB_DATABASE > /home/appuser/$DRIVE_PATH/$DATE/dump.sql

#File Backup
cp -rf /config/www/uploads /home/appuser/$DRIVE_PATH/$DATE/uploads

#Drive upload
echo "upload to drive: /$DRIVE_PATH/$DATE"
skicka mkdir -p /$DRIVE_PATH/$DATE
skicka upload /home/appuser/$DRIVE_PATH/$DATE/dump.sql /$DRIVE_PATH/$DATE/dump.sql
skicka upload /home/appuser/$DRIVE_PATH/$DATE/uploads /$DRIVE_PATH/$DATE/uploads

FOLDERS=($(skicka ls /$DRIVE_PATH))
NUM_FOLDERS=${#FOLDERS[@]}
echo "number of folders: $NUM_FOLDERS"
if [ "$NUM_FOLDERS" -gt "$MAX_BACKUP_NUMBER" ]; then
    NUM_FOLDERS_TO_REMOVE=$(expr $NUM_FOLDERS - $MAX_BACKUP_NUMBER)
    echo "number of folders to remove: $NUM_FOLDERS_TO_REMOVE"
    for i in $(seq 0 $(expr $NUM_FOLDERS_TO_REMOVE - 1));
    do
	TO_REMOVE=$DRIVE_PATH/${FOLDERS[$i]}
	echo "removing folder: $TO_REMOVE"
	skicka rm -s -r /$TO_REMOVE
	rm -rf /home/appuser/$TO_REMOVE
    done
fi

