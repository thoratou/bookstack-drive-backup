# bookstack-drive-backup

The aim of this project is to automate Bootslack backups on Google Drive using skicka project:
https://github.com/google/skicka

Please note it means to be working with the docker version of Bootslack:
https://www.bookstackapp.com/docs/admin/installation/#docker

# Local Development

## Setup

Create the .env file and set all required variables:

```
DB_USER=<yourdbuser>
DB_PASSWORD=<yourdbpass>
DB_DATABASE=bookstackapp
PUID=1000
PGID=1000
```

Then run the init script:

```
make init
```

Follow skicka synchronization to your google account procedure. 
It will also create skicka metadata files.

Then start bootslack dockers (should be named bootslack and bootslack_db)

At last run the foolowing command to start your local container:

```
make
```

If you have changes to apply, kill the docker instance, make your update and rerun make command.

# Integration in docker-compose.yml

Typical docker-compose.yml with bootslack and attached backup container:

```
---
version: "3"
services:
  bookstack:
    image: linuxserver/bookstack
    container_name: bookstack
    environment:
      - PUID=1000
      - PGID=1000
      - DB_HOST=bookstack_db
      - DB_USER=<yourdbuser>
      - DB_PASS=<yourdbpass>
      - DB_DATABASE=bookstackapp
    volumes:
      - /home/<youruser>/bookstack/data:/config
    ports:
      - 6875:443
    restart: unless-stopped
    depends_on:
      - bookstack_db
  bookstack_db:
    image: linuxserver/mariadb
    container_name: bookstack_db
    environment:
      - PUID=1000
      - PGID=1000
      - MYSQL_ROOT_PASSWORD=<yourdbpass>
      - TZ=Europe/London
      - MYSQL_DATABASE=bookstackapp
      - MYSQL_USER=<yourdbuser>
      - MYSQL_PASSWORD=<yourdbpass>
    volumes:
      - /home/<youruser>/bookstack/data:/config
    restart: unless-stopped
  bookstack_backup:
    image: bookstack-backup
    container_name: bookstack_backup
    environment:
      - PUID=1000
      - PGID=1000
      - DB_USER=<yourdbuser>
      - DB_PASSWORD=<yourdbpass>
      - DB_DATABASE=bookstackapp
      - CRON_FREQUENCY=0 4 * * *
      - MAX_BACKUP_NUMBER=30
      - DRIVE_PATH=bookstack.backup
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /home/<youruser>/bookstack/data:/config:ro
      - /home/<youruser>/bookstack/backup:/home/appuser/bookstack.backup
      - /home/<youruser>/bookstack/skicka:/home/appuser_skicka
    restart: unless-stopped
    depends_on:
      - bookstack_db
      - bookstack
```