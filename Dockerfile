############################
# STEP 1 build executable binary
############################
FROM golang:1.13-alpine as skicka-builder

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

#RUN CGO_ENABLED=0 go get -a -ldflags '-s' github.com/google/skicka

RUN go get -d github.com/google/skicka
RUN cd "${GOPATH}/src/google.golang.org/" && rm -rf grpc && git clone https://github.com/grpc/grpc-go grpc
RUN cd "${GOPATH}/src/google.golang.org/grpc" && git checkout 69baa3f1924e05f3de0a4891ffd5ed325bbc0eaf && go build
RUN cd "${GOPATH}/src/github.com/google/skicka" && go install

############################

FROM golang:1.13-alpine as supercronic-builder

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

RUN go get -u github.com/golang/dep/cmd/dep

RUN go get -d github.com/aptible/supercronic \
    && cd "${GOPATH}/src/github.com/aptible/supercronic" \
    && dep ensure -vendor-only \
    && go install

############################
# STEP 2 build a small image
############################
FROM alpine:latest

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Copy our static executable
COPY --from=skicka-builder /go/bin/skicka /usr/bin/skicka

# Copy supercronic
COPY --from=supercronic-builder /go/bin/supercronic /usr/bin/supercronic

# Copy docker client
COPY --from=yobasystems/alpine-docker /usr/bin/docker /usr/bin/docker

# su-exec
RUN apk --no-cache add su-exec
RUN chmod u+s /sbin/su-exec

# Bash
RUN apk --no-cache add bash

#Home dir
WORKDIR /home/appuser_skicka

# Copy script
RUN mkdir /home/appuser
COPY backup.sh /home/appuser/backup.sh
COPY docker-entrypoint.sh /home/appuser/docker-entrypoint.sh

# Copy cron
RUN mkdir /home/appuser/cron
COPY crontabs.tpl /home/appuser/cron/crontabs

# Log folder
RUN mkdir /home/appuser/log && touch /home/appuser/log/cron.log

ENTRYPOINT ["sh", "/home/appuser/docker-entrypoint.sh"]

CMD sed -i "s/{{ CRON_FREQUENCY }}/$CRON_FREQUENCY/g" /home/appuser/cron/crontabs \
    && supercronic /home/appuser/cron/crontabs