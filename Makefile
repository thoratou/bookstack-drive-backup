REPO_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
USER = $(shell echo $$USER)

.PHONY: all
all: build run

.PHONY: build
build:
	@printf "\033[32m Local build minecraft-rcon-service\n\033[0m"
	@docker build -t bookstack-backup .

.PHONY: init
init:
	@printf "\033[32m Init skicka\n\033[0m"
	@mkdir -p $(REPO_ROOT)/skicka
	@docker run -it --rm \
		--name bookstack-backup-test \
		--env-file=.env \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		bookstack-backup su-exec appuser skicka init
	@docker run -it --rm \
		--env-file=.env \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		bookstack-backup su-exec appuser skicka -no-browser-auth ls

.PHONY: ls
ls:
	@printf "\033[32m Display drive root content\n\033[0m"
	@mkdir -p $(REPO_ROOT)/skicka
	@docker run -it --rm \
		--name bookstack-backup-test \
		--env-file=.env \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		bookstack-backup su-exec appuser skicka ls

.PHONY: run
run:
	@printf "\033[32m Run local minecraft-rcon-service\n\033[0m"
	@mkdir -p $(REPO_ROOT)/bookstack.backup
	@docker run -it --rm \
		--name bookstack-backup-test \
		--env-file=.env \
		-e CRON_FREQUENCY="* * * * *" \
		-e MAX_BACKUP_NUMBER=2 \
		-e DRIVE_PATH=bookstack.backup.test \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /home/$(USER)/bookstack/data:/config:ro \
		-v $(REPO_ROOT)/bookstack.backup:/home/appuser/bookstack.backup.test \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		bookstack-backup

.PHONY: bash
bash:
	@printf "\033[32m Open bash on existing container\n\033[0m"
	@docker exec -it bookstack-backup-test su-exec appuser bash


.PHONY: run-bash
run-bash:
	@printf "\033[32m Run local minecraft-rcon-service\n\033[0m"
	@mkdir -p $(REPO_ROOT)/bookstack.backup
	@docker run -it --rm \
		--name bookstack-backup-test \
		--env-file=.env \
		-e CRON_FREQUENCY="* * * * *" \
		-e MAX_BACKUP_NUMBER=2 \
		-e DRIVE_PATH=bookstack.backup.test \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v /home/$(USER)/bookstack/data:/config:ro \
		-v $(REPO_ROOT)/bookstack.backup:/home/appuser/bookstack.backup.test \
		-v $(REPO_ROOT)/skicka:/home/appuser_skicka \
		bookstack-backup su-exec appuser bash
